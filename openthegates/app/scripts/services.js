/*
 * Created by: Michiel Gulickx
 * Date: 01-07-2015
 * Name: services.js
 * Description: Open The Gates opdracht
 *
 * Totaal inwoners: http://datatank4.gent.be//bevolking/totaalaantalinwoners.json
 * Bevolkingsdichtheid: http://datatank4.gent.be//bevolking/bevolkingsdichtheidperwijk.json
 * Geboortes: http://datatank4.gent.be//bevolking/geboortes.json
 * Overlijdens: http://datatank4.gent.be/bevolking/overlijdensperduizend.json
 * Secundaire scholen: http://datatank4.gent.be//onderwijsopvoeding/secundairescholen.json
 * Parkeergarages: http://datatank.gent.be/Mobiliteitsbedrijf/Parkings.json
 * Gentse Feesten locaties: http://datatank4.gent.be//cultuursportvrijetijd/gentsefeestenlocaties.json
 * Wijkcomfort: http://datatank4.gent.be//wonen/comfortniveauwoningen.json
 * Bevolking per leeftijd/geslacht: http://datatank4.gent.be/bevolking/bevolkingperleeftijdgeslacht19992011.json
 * OCMWbureaus: http://datatank4.gent.be/welzijn/welzijnsbureausocmw.json
 */

//Hier worden alle JSON files opgehaald en in de html geplaatst.
//Parkeergarages


$('#parkeergarages').one("click",function(){
    $.ajax({
        url: 'http://datatank.gent.be/Mobiliteitsbedrijf/Parkings.json',
        dataType: 'json',
        type: 'get',
        cache: false,
        success: function(data) {

        $(data.Parkings.parkings).each(function(index, value) {

            // Berekenen van parkeerplaatsen
            total = parseInt(value.totalCapacity);
            available = parseInt(value.availableCapacity);
            taken = total - available;
            percentage = taken * 100;
            percentage = percentage / total;
            percentage_color = percentage * 2;



            // Adres klaarmaken voor Googlemaps
            str = value.address;
            replaced = str.replace(/ /g, '+');
            address = replaced.replace(',','+');
            address = address.replace('<br>','');



            $('#datawrap').append(
                '<div class="dataond col col-xxs-12 col-md-3"> <h3> ' + value.description + '</h3><p>' + value.address + '<p>Totaal aantal plaatsen</p>' + '<label>' +  value.totalCapacity + '</label>' + '<p>Vrije plaatsen</p>' + '<div id="capacity"><div id="taken" style="width:' + percentage + '%; height:100%";"> </div></div> <label>' + value.availableCapacity + '</label>' + '<div id="maps">' + "<iframe width='100%' height='100%' frameborder='0' scrolling='no'  marginheight='0' marginwidth='0'   src='https://maps.google.com/maps?&amp;q="+ encodeURIComponent( address ) +"&amp;output=embed'></iframe>" + '</div>' + '</p></div>'
            );


        });
    }
    });
});



//Inwoners per wijk
$('#inwoners').one("click",function(){
$.ajax({
    url: 'http://datatank4.gent.be//bevolking/totaalaantalinwoners.json',
    dataType: 'json',
    type: 'get',
    cache: false,
    success: function(data) {

        $(data).each(function(index, value) {

            wijkname = value.wijk;
            year = value.year_2010;


            $('#datawrap2').append(
                '<div class="dataond col col-xxs-12 col-md-3"> <h3> ' +
                wijkname +
                '</h3><p>' +
                year +
                '<div id="maps">' +
                "<iframe width='100%' height='100%' frameborder='0' scrolling='no'  marginheight='0' marginwidth='0'   src='https://maps.google.com/maps?&amp;q="+ encodeURIComponent( wijkname + ' ' + 'Gent' ) +"&amp;output=embed'></iframe>" +
                '</div>' +


                '</p></div>'
            );

        });
    }
});
});

//Geboortes per wijk
$('#geboortes').one("click",function(){
$.ajax({
    url: 'http://datatank4.gent.be//bevolking/geboortes.json',
    dataType: 'json',
    type: 'get',
    cache: false,
    success: function(data) {

        $(data).each(function(index, value) {

            wijkname = value.wijk;
            year = value.year_2009;


            $('#datawrap3').append(
                '<div class="dataond col col-xxs-12 col-md-3"> <h3> ' +
                wijkname +
                '</h3><p>' + '<p>Totaal aantal geboortes in 2010:</p>' +
                year +
                '<div id="maps">' +
                "<iframe width='100%' height='100%' frameborder='0' scrolling='no'  marginheight='0' marginwidth='0'   src='https://maps.google.com/maps?&amp;q="+ encodeURIComponent( wijkname + ' ' + 'Gent' ) +"&amp;output=embed'></iframe>" +
                '</div>' +
                '</p></div>'
            );
        });
    }
});
});

//Bevolkingsdichtheid per wijk

$('#bevolkingsdichtheid').one("click",function(){
$.ajax({
    url: 'http://datatank4.gent.be//bevolking/bevolkingsdichtheidperwijk.json',
    dataType: 'json',
    type: 'get',
    cache: false,
    success: function(data) {

        $(data).each(function(index, value) {

            wijkname = value.wijk;
            year = value.year_2010;


            $('#datawrap4').append(
                '<div class="dataond col col-xxs-12 col-md-3"> <h3> ' +
                wijkname +
                '</h3><p>' + '<p>Bevolkingsdichtheid per km2</p>' +
                year +
                '<div id="maps">' +
                "<iframe width='100%' height='100%' frameborder='0' scrolling='no'  marginheight='0' marginwidth='0'   src='https://maps.google.com/maps?&amp;q="+ encodeURIComponent( wijkname + ' ' + 'Gent' ) +"&amp;output=embed'></iframe>" +
                '</div>' +
                '</p></div>'
            );




        });
    }
});
});


//Secundaire scholen
$('#scholen').one("click",function(){
$.ajax({
    url: 'http://datatank4.gent.be//onderwijsopvoeding/secundairescholen.json',
    dataType: 'json',
    type: 'get',
    cache: false,
    success: function(data) {

        $(data).each(function(index, value) {

            schoolnaam = value.NAAM;
            plaats = value.GEMEENTE;
            straat = value.STRAAT + ' ' + value.HUISNR;


            $('#datawrap5').append(
                '<div class="dataond col col-xxs-12 col-md-3"> <h3> ' +
                schoolnaam +
                '</h3><p>'  +
                plaats + '</p>' + '<p>' + straat + '</p>' +
                '<div id="maps">' +
                "<iframe width='100%' height='100%' frameborder='0' scrolling='no'  marginheight='0' marginwidth='0'   src='https://maps.google.com/maps?&amp;q="+ encodeURIComponent( schoolnaam ) +"&amp;output=embed'></iframe>" +
                '</div>' +
                '</p></div>'
            );




        });
    }
});
});

//Feestlocaties Gentse feesten
$('#feesten').one("click",function(){
$.ajax({
    url: 'http://datatank4.gent.be//cultuursportvrijetijd/gentsefeestenlocaties.json',
    dataType: 'json',
    type: 'get',
    cache: false,
    success: function(data) {

        $(data).each(function(index, value) {

            locatie = value.naam;

            console.log('ge zit erin');


            $('#datawrap6').append(
                '<div class="dataond col col-xxs-12 col-md-3"> <h3> ' +
                locatie +
                '</h3>'  +
                '<div id="maps">' +
                "<iframe width='100%' height='100%' frameborder='0' scrolling='no'  marginheight='0' marginwidth='0'   src='https://maps.google.com/maps?&amp;q="+ encodeURIComponent( locatie + ' ' + 'Gent' ) +"&amp;output=embed'></iframe>" +
                '</div>' +
                '</p></div>'
            );




        });
    }
});
});

//Wijkcomfort
$('#wijkcomfort').one("click",function(){
    $.ajax({
        url: 'http://datatank4.gent.be//wonen/comfortniveauwoningen.json',
        dataType: 'json',
        type: 'get',
        cache: false,
        success: function(data) {

            $(data).each(function(index, value) {

                locatie = value.wijk;
                grootcomfort = value.aantal_groot_comfort_2001;
                kleincomfort = value.aantal_klein_comfort_2001;
                zondercomfort = value.aantal_zonder_comfort_2001;

                $('#datawrap7').append(
                    '<div class="dataond col col-xxs-12 col-md-3"> <h3> ' +
                    locatie +
                    '</h3><p>' +
                    'Huizen met groot comfort: ' + grootcomfort +'</p>' + '<p>' + 'Huizen met weinig comfort: ' + kleincomfort + '</p>' + '<p>' + 'Huizen zonder comfort: ' + zondercomfort + '</p>' +
                    '</p>' +
                    '<div id="maps">' +
                    "<iframe width='100%' height='100%' frameborder='0' scrolling='no'  marginheight='0' marginwidth='0'   src='https://maps.google.com/maps?&amp;q="+ encodeURIComponent( locatie + ' ' + 'Gent' ) +"&amp;output=embed'></iframe>" +
                    '</div>' +
                    '</p></div>'
                );




            });
        }
    });
});

//Overlijdens per duizend per wijk

$('#overlijdens').one("click",function(){
    $.ajax({
        url: 'http://datatank4.gent.be/bevolking/overlijdensperduizend.json',
        dataType: 'json',
        type: 'get',
        cache: false,
        success: function(data) {

            $(data).each(function(index, value) {

                wijkname = value.wijk;
                year = value.year_2009;


                $('#datawrap8').append(
                    '<div class="dataond col col-xxs-12 col-md-3"> <h3> ' +
                    wijkname +
                    '</h3><p>' + '<p>Overlijdens per duizend in 2009</p>' +
                    year +
                    '<div id="maps">' +
                    "<iframe width='100%' height='100%' frameborder='0' scrolling='no'  marginheight='0' marginwidth='0'   src='https://maps.google.com/maps?&amp;q="+ encodeURIComponent( wijkname + ' ' + 'Gent' ) +"&amp;output=embed'></iframe>" +
                    '</div>' +
                    '</p></div>'
                );




            });
        }
    });
});

//WIJKMIGRATIE
$('#wijkmigratie').one("click",function(){
    $.ajax({
        url: 'http://datatank4.gent.be/bevolking/wijkmigratie.json',
        dataType: 'json',
        type: 'get',
        cache: false,
        success: function(data) {

            $(data).each(function(index, value) {

                wijk = value.wijk;
                jaar = value.year_2009;

                $('#datawrap9').append(
                    '<div class="dataond col col-xxs-12 col-md-3"> <h3> ' +
                    wijk +
                    '<p>' +
                     'Aantal verhuizingen:' +
                     '</p>' +
                    '</h3><p>' +
                    jaar +
                    '<div id="maps">' +
                    "<iframe width='100%' height='100%' frameborder='0' scrolling='no'  marginheight='0' marginwidth='0'   src='https://maps.google.com/maps?&amp;q="+ encodeURIComponent( wijk + ' ' + 'Gent' ) +"&amp;output=embed'></iframe>" +
                    '</div>' +


                    '</p></div>'
                );

            });
        }
    });
});

//OCMWbureau's
$('#ocmw').one("click",function(){
    $.ajax({
        url: 'http://datatank4.gent.be/welzijn/welzijnsbureausocmw.json',
        dataType: 'json',
        type: 'get',
        cache: false,
        success: function(data) {

            $(data).each(function(index, value) {

                gemeente = value.gemeente;
                naam = value.naam;
                straat = value.straat;
                huisnummer= value.huisnummer;
                postcode = value.postcode;

                $('#datawrap10').append(
                    '<div class="dataond col col-xxs-12 col-md-3"> <h3> ' +
                    naam +
                    '<p>' +
                    straat + ' ' + huisnummer +
                    '</p>' +
                    '<p>' +
                    postcode + ' ' + 'Gent' +
                    '</p>' +
                    '</h3><p>' +
                    //jaar +
                    '<div id="maps">' +
                    "<iframe width='100%' height='100%' frameborder='0' scrolling='no'  marginheight='0' marginwidth='0'   src='https://maps.google.com/maps?&amp;q="+ encodeURIComponent( straat + huisnummer + ' ' + 'Gent' ) +"&amp;output=embed'></iframe>" +
                    '</div>' +


                    '</p></div>'
                );

            });
        }
    });
});

