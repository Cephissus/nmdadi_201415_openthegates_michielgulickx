

var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var cssMin = require('gulp-css');
var jsvalidate = require('gulp-jsvalidate');

gulp.task('css', function() {
    gulp.src([
        'app/styles/app.css',
        'app/styles/buttons.css',
        'app/styles/common.css',
        'app/styles/helpers.css',
        'app/styles/layout.css',
        'app/styles/lists.css',
        'app/styles/normalize.css',
        'app/styles/pages.css'
    ])

        .pipe(concat('combined.css'))
        .pipe(cssMin())
        .pipe(gulp.dest('dist'));
});

gulp.task('scripts', function() {
    gulp.src([
        'app/scripts/app.js',
        'app/scripts/helpers.js',
        'app/scripts/services.js',
        'app/scripts/utilities.js',
    ])



        .pipe(concat('combinedjs.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist'));

    gulp.src([
        'app/components/**/*.js'
    ])

        .pipe(concat('combinedcomponents.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist'));
});

gulp.task('default', function () {
    return gulp.src('app/scripts/app.js')
        .pipe(jsvalidate());
});

gulp.task('default',['css','scripts']);