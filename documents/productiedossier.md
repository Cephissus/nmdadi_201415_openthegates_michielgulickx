#Productiedossier
##Michiel Gulickx - 2 MMP b
##Briefing en analyse
###1. Wat wil de klant?
De klant wenst een webapplicatie waarop hij veel informatie kan vinden omtrent informatie over de stad Gent. Hij wil de beschikbare parkeerplaatsen in grote garages kunnen nakijken. Hij wil bepaalde informatie kunnen opzoeken over de bevolking van de stad Gent.
###2. Wat is de boodschap?
De boodschap is een nette simplistische user-interface. De gebruiker moet genoeg informatie krijgen over bepaalde aspecten van de stad gent, zonder overweldigd te zijn door de hoeveelheid informatie die beschikbaar is op de website. Aan de hand van een simpele gebruiksvriendelijke webapp is dit mogelijk.
###3. Wie is het doelpubliek?
* Demografisch: Van 16 jaar tot 70 jaar, in principe internet- en smartphonegebruikers in het algemeen. Senioren die geïnteresseerd zijn in hun stad worden echter ook aangesproken.
* Psychografisch: Mensen die geïnteresseerd zijn in hun stad.
* Technografisch: Iedereen die af en toe op internet actief is, hoeft niet een grote technologie-interesse te hebben.
###4. Welke informatie is er voorhanden?
Er is zeer veel informatie voor handen. Stad Gent voorziet ontzettend veel datasets die iedereen mag gebruiken en aanspreken.
###5. Wat is de stand van zaken?
Er zijn nog niet veel applicaties die zich focussen op het inladen van JSON datasets. 
###6. Keuze technologie
#####Front-end webapplicatie aan de hand van:
* HTML5
* CSS3
* Javascript
###7. Tijdspanne
De web-applicatie moet ingeleverd worden op 16 augustus 2015.
___

# PERSONA'S #

## Vanessa Mertens
* Leeftijd: 22
* Beroep: Studente
* Status: single
* Locatie: Gent

Bio: Vanessa woont in Wetteren en zit op kot in Gent. Ze studeert geschiedenis en is geïnteresseerd in de bevolkingsinformatie van Gent. Ze gaat graag uit maar is ook zeer gedreven in haar studies.


### Motivaties
1. Succes
1. Groei
1. Sociaal


---

## Peter Van Looveren
* Leeftijd: 31
* Beroep: bediende bij KBC
* Status: Getrouwd
* Locatie: Gent

Bio: Peter werkt al 10 jaar bij KBC. Hij is zeer goed met technologie en houdt van sociale netwerksites. Hij is een zeer actief smartphonegebruiker en is op de hoogte van de nieuwste apps.


### Motivaties
1. Succes
1. Groei
1. Macht

---

## Rita Dekkers
* Leeftijd: 63 jaar
* Beroep: Gepensioneerd
* Status: Getrouwd
* Locatie: Gent

Bio: Rita is een gepensioneerde vrouw woonachtig te Ledeberg in Gent. Ze woont al heel haar leven in Gent en gaat elke dag een koffietje drinken in haar vaste café in Ledeberg. Ze is niet zo vaardig met technologie en internet.

### Motivaties
1. Rust
1. Geluk
1. Vriendschap


---

## Technische specificaties
De webapp is opgebouwd aan de hand van de nieuwste front-end webtechnologieën. De site is opgebouwd met HTML5, CSS3 en javascript. Meer bepaald zijn er templates in de html die gestijld zijn met css, en aan de hand van jquery worden de datasets ingeladen op de juiste plaats. De navigatie van de webapp wordt geregeld door crossroads, hasher en handlebars.

##Functionele specificaties
Functionele specificaties:
Concreet is de bedoeling van de webapp om datasets (gevonden op de datasetwebsites van Stad Gent) in te laden, ze te stijlen, en ze weer te geven met aanvulling van Google Maps. Zo kan de user de webapp openen, een bepaalde dataset kiezen, en krijgt hij/zij een overzicht van de gekozen informatie met aanvulling van de locatie op Google Maps.
___
# Besluit
Over het algemeen kan ik zeggen dat ik zeer tevreden ben over het resultaat van deze opdracht. 
Ik ben vroeg begonnen aan de opdracht waardoor ik tijd had om genoeg informatie op te zoeken en een stijl en concept te bedenken voor de webapplicatie.  Nadat ik het concept bepaald had ben ik op zoek gegaan naar interessante datasets. Ik koos ervoor om voornamelijk datasets te gebruiken die betrekking hebben op de bevolking in Gent.

Het resultaat is een cleane webapp die gemakkelijk te gebruiken is en waar veel interessante informatie opstaat. Ik heb voldaan aan alle gevraagde technologieën en aspecten van de opdracht en ben dus zeer tevreden over het eindresultaat.


