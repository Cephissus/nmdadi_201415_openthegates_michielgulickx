## Synopsis

This webapp focusses on loading JSON-datasets into the html. I used datasets found on the datasets provided by the city of Ghent.

## Motivation

This webapp is an assignment for the course of NMDAD-I at Grafische and Digitale Media at the Artevelde University College Ghent.