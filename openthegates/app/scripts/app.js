/*
* Created by: Michiel Gulickx
 * Date: 01-07-2015
* Name: app.js
* Description: Open The Gates opdracht
*
* Totaal inwoners: http://datatank4.gent.be//bevolking/totaalaantalinwoners.json
* Bevolkingsdichtheid: http://datatank4.gent.be//bevolking/bevolkingsdichtheidperwijk.json
* Geboortes: http://datatank4.gent.be//bevolking/geboortes.json
* Overlijdens: http://datatank4.gent.be/bevolking/overlijdensperduizend.json
* Secundaire scholen: http://datatank4.gent.be//onderwijsopvoeding/secundairescholen.json
* Parkeergarages: http://datatank.gent.be/Mobiliteitsbedrijf/Parkings.json
* Gentse Feesten locaties: http://datatank4.gent.be//cultuursportvrijetijd/gentsefeestenlocaties.json
* Wijkcomfort: http://datatank4.gent.be//wonen/comfortniveauwoningen.json
*
*
*/
(function(){
    var App = {
        init:function(){
            //Create Handlebars Cache for templates
            this.hbsCache = {};
            //Cache the most important elements
            this.cacheElements();
            //Bind Events to the most important elements
            this.bindEvents();
            //Setup the routes
            this.setupRoutes();
            //Render the interface
            this.render();
        },

        cacheElements:function(){
            /* Cache Handlebars Templates
            * Vets, ...
            */
            if(!this.hbsCache['dataset1']){
                var src = document.querySelector('#dataset1-template').innerHTML;
                this.hbsCache['dataset1'] = Handlebars.compile(src);
            }
            if(!this.hbsCache['dataset2']){
                var src = document.querySelector('#dataset2-template').innerHTML;
                this.hbsCache['dataset2'] = Handlebars.compile(src);
            }
            if(!this.hbsCache['dataset3']){
                var src = document.querySelector('#dataset3-template').innerHTML;
                this.hbsCache['dataset3'] = Handlebars.compile(src);
            }
            if(!this.hbsCache['dataset4']){
                var src = document.querySelector('#dataset4-template').innerHTML;
                this.hbsCache['dataset4'] = Handlebars.compile(src);
            }
            if(!this.hbsCache['dataset5']){
                var src = document.querySelector('#dataset5-template').innerHTML;
                this.hbsCache['dataset5'] = Handlebars.compile(src);
            }
            if(!this.hbsCache['dataset6']){
                var src = document.querySelector('#dataset6-template').innerHTML;
                this.hbsCache['dataset6'] = Handlebars.compile(src);
            }
            if(!this.hbsCache['dataset7']){
                var src = document.querySelector('#dataset7-template').innerHTML;
                this.hbsCache['dataset7'] = Handlebars.compile(src);
            }
            if(!this.hbsCache['dataset8']){
                var src = document.querySelector('#dataset8-template').innerHTML;
                this.hbsCache['dataset8'] = Handlebars.compile(src);
            }
            if(!this.hbsCache['dataset9']){
                var src = document.querySelector('#dataset9-template').innerHTML;
                this.hbsCache['dataset9'] = Handlebars.compile(src);
            }
            if(!this.hbsCache['dataset10']){
                var src = document.querySelector('#dataset10-template').innerHTML;
                this.hbsCache['dataset10'] = Handlebars.compile(src);
            }
            if(!this.hbsCache['contact']){
                var src = document.querySelector('#contact-template').innerHTML;
                this.hbsCache['contact'] = Handlebars.compile(src);
            }

            /* DOM Elements
            * Vets List, ...
            */
            this.offcanvasNavigation = document.querySelector('.offcanvas-navigation');
            this.offcanvasToggles = document.querySelectorAll(".offcanvas-toggle");
            this.vetsList =  document.querySelector('#vets-list');
            this.dogsrunningplacesList =  document.querySelector('#runningplaces-list');
            this.dogstoiletsList =  document.querySelector('#toilets-list');
            this.nearestList = document.querySelector('#nearest-list');
        },

        bindEvents:function(){
            //Get the reference to this App
            //Fix this in nested Event Listeners by use of self or that
            var self = this;

            _.each(this.offcanvasToggles, function(toggle){
                toggle.addEventListener("click", function(ev){
                    ev.preventDefault();

                    var navigation_reference_name = this.getAttribute("href");
                    var navigation = document.querySelector(navigation_reference_name);
                    document.body.classList.toggle('offcanvasactive');

                    return false;
                }, false);
            });
        },

        setupRoutes:function(){
            this.router = crossroads;//Clone the crossroads object
            this.hash = hasher;//Clone the hasher object

            //Get the reference to this App
            //Fix this in nested Event Listeners by use of self or that
            var self = this;

            //Crossroads settings
            var homeRoute = this.router.addRoute('/',function(){
                //Set the active page where id is equal to the route
                self.setActivePage('home');
                //Set the active menuitem where href is equal to the route
                self.setActiveNavigationLink('home');
            });
            var sectionRoute = this.router.addRoute('/{section}');//Add the section route to crossroads
            sectionRoute.matched.add(function(section){
                //Set the active page where id is equal to the route
                self.setActivePage(section);
                //Set the active menuitem where href is equal to the route
                self.setActiveNavigationLink(section);
            });//Hash matches to section route

            //Hash settings
            this.hash.initialized.add(function(newHash, oldHash){self.router.parse(newHash);});//Parse initial hash
            this.hash.changed.add(function(newHash, oldHash){self.router.parse(newHash);document.body.classList.remove('offcanvasactive');});//Parse changes in the hash
            this.hash.init();//Start listening to the hashes
        },
        setActivePage:function(section){
            //Get the reference to this App
            //Fix this in nested Event Listeners by use of self or that
            var self = this;

            //Set the active page where id is equal to the route
            var pages = document.querySelectorAll('.page');
            if(pages != null && pages.length > 0){
                _.each(pages,function(page){
                    if(page.id == section){
                        page.classList.add('active');
                        var pageStatesNavigation = page.querySelector('.page-state-navigation');

                        if(pageStatesNavigation){
                            var as = pageStatesNavigation.querySelectorAll('ul>li>a');
                            _.each(as, function(a){
                                a.addEventListener('click', function(ev){
                                    ev.preventDefault();

                                    self.setActivePageState(this.parentNode.dataset.page, this.parentNode.dataset.state);

                                    return false;
                                });
                            });
                        }



                    }else{
                        page.classList.remove('active');
                    }
                });
            }
        },
        setActiveNavigationLink:function(section){
            //Set the active menuitem where href is equal to the route
            var navLinks = document.querySelectorAll('.offcanvas-navigation ul li a');
            if(navLinks != null && navLinks.length > 0){
                var effLink = '#/' + section;
                _.each(navLinks,function(navLink){
                    if(navLink.getAttribute('href') == effLink){
                        navLink.parentNode.classList.add('active');
                    }else{
                        navLink.parentNode.classList.remove('active');
                    }
                });
            }
        },
        setActivePageState:function(page, state){
            var pageStates = document.querySelectorAll('#' + page + ' .page-state')
            if(pageStates != null && pageStates.length > 0){
                _.each(pageStates,function(pageState){
                    if(pageState.dataset.state == state){
                        pageState.classList.add('active');
                    }else{
                        pageState.classList.remove('active');
                    }
                });
            }

            var pageStatesNavigation = document.querySelectorAll('[data-page=' + page + ']');
            if(pageStatesNavigation != null && pageStatesNavigation.length > 0){
                _.each(pageStatesNavigation,function(pageStateNav){
                    if(pageStateNav.dataset.state == state){
                        pageStateNav.classList.add('active');
                    }else{
                        pageStateNav.classList.remove('active');
                    }
                });
            }
        }

    };

    App.init();//Initialize the application
})();